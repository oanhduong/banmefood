const http = require('http')
const express = require('express')
const path = require('path')
const compression = require('compression')

const app = express()
app.use(express.static(__dirname + '/public'))
app.use(compression())
app.get('*', (req, res) => res.sendFile(path.join(__dirname, 'public', 'home.html')))

// const privateKey = fs.readFileSync(path.join(__dirname, 'sslcert', 'server.key'), 'utf8')
// const certificate = fs.readFileSync(path.join(__dirname, 'sslcert', 'server.crt'), 'utf8')
// const credentials = { key: privateKey, cert: certificate }

const httpServer = http.createServer(app)
// const httpsServer = https.createServer(credentials, app)

httpServer.listen(80, (err) => {
  if (err) {
    console.error(`Start server error`, err)
  } else {
    console.log(`Server is listening on port 80`)
  }
})

// httpsServer.listen(443, (err) => {
//   if (err) {
//     console.error(`Start server error`, err)
//   } else {
//     console.log(`Server is listening on port 443`)
//   }
// })
