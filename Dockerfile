# Use an official Node runtime as a parent image
FROM node:12.7.0-alpine

RUN mkdir -p /usr/src/app

COPY . /app/server
WORKDIR /app/server
RUN npm install

ENV PORT=80
ENV NODE_ENV=production

EXPOSE $PORT
CMD ["node", "server.js"]

