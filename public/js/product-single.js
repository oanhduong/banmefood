
$(document).ready(function () {
  var products = [
    {
      name: 'Chai cao',
      price: '110k',
      image: 'product1.png',
      type: 'Chai nhựa',
      size: [500, 1000],
      slug: 'mat-ong-hoa-cafe-chai-nhua'
    },
    {
      name: 'Con ong',
      price: '110k',
      image: 'product3.png',
      type: 'Chai thuỷ tinh',
      size: [180, 350, 730],
      slug: 'mat-ong-hoa-cafe-chai-thuy-tinh-con-ong'
    },
    {
      name: 'Chai bầu',
      price: '110k',
      image: 'product4.png',
      type: 'Chai thuỷ tinh',
      size: [200, 350, 500],
      slug: 'mat-ong-hoa-cafe-chai-thuy-tinh-bau'
    },
    {
      name: 'Chai vuông',
      price: '110k',
      image: 'product2.png',
      type: 'Chai thuỷ tinh',
      size: [280, 380, 500, 730],
      slug: 'mat-ong-hoa-cafe-chai-thuy-tinh-vuong'
    },
    {
      name: 'Chai tròn',
      price: '110k',
      image: 'product5.png',
      type: 'Chai thuỷ tinh',
      size: [250, 300, 500, 1000],
      slug: 'mat-ong-hoa-cafe-chai-thuy-tinh-tron'
    }
]

  var getParam = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
  }

  var addProduct = () => {
    const slug = getParam('name')
    var product = products.find((e) => e.slug === slug)
    $('#slide100-01').slide100({
      autoPlay: "false",
      timeAuto: 3000,
      deLay: 400,

      linkIMG: [
      `images/${product.image}`
      ],

      linkThumb: [
        `images/${product.image}`
      ]
    })
  }

  addProduct()
})
