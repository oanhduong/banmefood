
$(document).ready(function () {
  var products = [
    {
      name: 'Con ong',
      price: '59,000 - 119,000 - 229,000',
      image: 'product3.png',
      type: 'Chai thuỷ tinh',
      size: [180, 350, 730],
      slug: 'mat-ong-hoa-cafe-chai-thuy-tinh-con-ong'
    },
    {
      name: 'Chai bầu',
      price: '119,000 - 149,000',
      image: 'product4.png',
      type: 'Chai thuỷ tinh',
      size: [350, 500],
      slug: 'mat-ong-hoa-cafe-chai-thuy-tinh-bau'
    },
    {
      name: 'Chai vuông',
      price: '149,000 - 229,000',
      image: 'product2.png',
      type: 'Chai thuỷ tinh',
      size: [500, 730],
      slug: 'mat-ong-hoa-cafe-chai-thuy-tinh-vuong'
    },
    {
      name: 'Chai tròn',
      price: '109,000',
      image: 'product5.png',
      type: 'Chai thuỷ tinh',
      size: [300],
      slug: 'mat-ong-hoa-cafe-chai-thuy-tinh-tron'
    },
    {
      name: 'Chai cao',
      price: '139,000',
      image: 'product1.png',
      type: 'Chai nhựa',
      size: [500],
      slug: 'mat-ong-hoa-cafe-chai-nhua'
    },
]
//onclick="window.location.href='product-single.html?name=${slug}'"
  var addProduct = ({name, price, image, type, size, slug}) => {
      $('#product-list').append(`
      <div class="col-sm-6 col-lg-6 p-b-30">
        <div class="block1">
            <div class="block1-bg wrap-pic-w bo-all-1 bocl12 hov3 trans-04 borad-24">
                <img class="borad-24" src="images/${image}" alt="IMG">
                <div class="">
                    <div class="p-t-32 p-b-32 p-l-24 p-r-24">
                        <div class="flex-w flex-sb-m m-b-16">
                            <a href="#" style="font-weight: bold; color: black;" class="txt-m-122 cl3 hov-cl10 trans-04 m-r-20 js-name1">
                                ${name}
                            </a>
                        </div>
                        <p><span style="color: black;" class="txt-s-110">${type}: ${size.map((e) => `${e}ml`).join(', ')}</span></p>
                        <p><span style="color: black;" class="txt-s-110">Giá: ${price} VNĐ</span></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
      `)
  }

  products.forEach(addProduct)
})
